#!usr/bin/env python
# coding: utf-8

u"""
    自动化, 最基础命令.

    usage: fab function_name -f fab_basic.py
"""

__author__ = 'kylinfish@126.com'
__date__ = '2013/8/15'

from fabric.api import *


def sys_update():
    u"""system update.
    """

    run('apt-get update')
    run('apt-get upgrade')


def my_command(str_cmd):
    u"""执行指定命令, 一定要引号引住.
    """

    run(str_cmd)


if __name__ == '__main__':
    u"""测试.
    """

    pass
