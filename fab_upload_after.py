#!usr/bin/env python
# coding: utf-8

u"""
    自动化部署, 本地到远程任务.

    usage:
        fab upload_after -f fab_upload_after.py
"""

__author__ = 'kylinfish@126.com'
__date__ = '2013/8/15'

from fabric.api import *


def nginx_site_restart():
    u"""nginx site restart.

        python: nginx + uwsgi
    """

    sudo('invoke-rc.d uwsgi restart')
    sudo('nginx -s reload')
    sudo('service nginx restart')


def apache_site_restart():
    u"""apache site restart.
    """

    sudo('')
    sudo('')
    sudo('')


def upload_after():
    """迭代文件上传之后.
    """

    # nginx_site_restart()
    pass


if __name__ == '__main__':
    u"""测试
    """

    pass
