#!usr/bin/env python
# coding: utf-8

u"""
    自动化部署, 本地到远程任务.

    usage: fab update_upload -f fab_upload.py
"""

__author__ = 'kylinfish@126.com'
__date__ = '2013/8/15'

import os
import six
from fabric.api import *

from service import env_ky, record_action_files, config_update_datetime
from config import remote_folder
from fab_upload_before import upload_before
from fab_upload_after import upload_after


def update_upload():
    u"""执行本地更新包的迭代操作.
    """

    # 配置起始点时间
    config_update_datetime(__file__)

    # 更新开始前
    upload_before()

    # 检索收集待上传文件
    with lcd(env_ky.app_path):  # 切换的自动部署脚本目录
        record_action_files()  # 检索更新文件，记录更新文件，以zip存档更新文件

    # 上传更新包文件到远程
    with lcd(env_ky.storage_path):  # 切换到更新包存档目录

        local('tar czf %s.tar.gz %s.zip' % (env_ky.gen_pkg_name, env_ky.gen_pkg_name))  # 压缩本地更新包
        put('%s.tar.gz' % env_ky.gen_pkg_name, remote_folder)  # 上传压缩包到远程目录

    # 远程文件解压覆盖
    with cd(remote_folder):  # 切换到远程目录
        run('tar zxf %s.tar.gz' % env_ky.gen_pkg_name)  # 远程解压
        run('unzip %s.zip' % env_ky.gen_pkg_name)  # 远程解压

        run('rm %s.tar.gz' % env_ky.gen_pkg_name)  # 远程删除
        run('rm %s.zip' % env_ky.gen_pkg_name)  # 远程删除

    # 更新完成后
    if env_ky.dynamic_file_exist:
        # 如果有动态文件, 重启web站点
        # 如果只是模板html, js, css, 图片等文件, 就勿需重启啦
        upload_after()


def update_rollback():
    u"""更新发布失败, 执行回滚.

        以项目的父级目录操作.
    """

    remote_project_parent_dir = os.path.dirname(remote_folder)

    # 上次发布前的备份文件
    bck_log = None
    try:
        bck_log = open('%s/backup_remote.log' % env_ky.app_path, 'rb')
        # 取文件记录的最后一行
        backup_project_file_name = str(bck_log.readlines()[-1].replace("\n", ""))
    except:
        raise
    finally:
        if bck_log:
            bck_log.close()

    # 远程文件解压覆盖
    with cd(remote_project_parent_dir):  # 切换到远程目录
        run('tar zxf %s' % backup_project_file_name)  # 远程解压
        run('rm %s' % backup_project_file_name)  # 远程删除

    # 完成后重启
    upload_after()


if __name__ == '__main__':
    u"""单元测试.
    """

    six.print_("beginning ...")
    six.print_(env_ky.app_path)
    six.print_(env_ky.project_path)
    pass
