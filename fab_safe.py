#!usr/bin/env python
# coding: utf-8

"""
自动化部署, python代码保护机制.

usage:
    from fab_safe import decryption_file
    from fab_safe import encryption_file
"""

__author__ = 'kylinfish@126.com'
__date__ = '2013/8/15'

import os
import sys
import six
import time
import random
import os.path
import getpass

# import service
from service import env_ky, search_match_files, config_update_datetime
from encrypted import EncryptedFile


def decryption_file(pyc_file):
    u"""uncompyle2 反编译pyc文件.
    """

    six.print_('decomiler beging init.')

    un_path = sys.executable
    un_path = un_path[0:un_path.rfind(os.sep)]
    new_name = '%s.py' % os.path.splitext(pyc_file)[0]

    command = "python -u " + un_path + "\scripts\uncompyle2 " + pyc_file + ">" + new_name
    try:
        os.system(command)
    except Exception as e:
        six.print_(e)
        six.print_(pyc_file)

    six.print_('decomiler have finished.')


def encryption_file(pyc_file, password=None):
    u"""加密Pyc文件，防止被反编译.
    """

    pyc_file_data = open(pyc_file, 'rb').readlines()

    my_list = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
    rd_slice = ''.join(random.sample(my_list, 6))  # 随机取6位
    cpu_time = '%010d' % int(time.clock() * 10 ** 10)  # cpu时间取10位数

    if password:
        password = '%s%s%s' % (password, rd_slice, cpu_time)

    f = EncryptedFile(pyc_file, passphrase=password)

    f.writelines(pyc_file_data)
    # f.write("super secret message")
    f.close()


def encrypt_log_file():
    u"""压缩文件并记入到日志文件.
    """

    if not os.path.exists(env_ky.storage_path):
        os.makedirs(env_ky.storage_path)
    else:
        if os.path.ismount(env_ky.storage_path) or not os.path.isdir(env_ky.storage_path):
            six.print_(u'亲, 指定的存储路径错误！')
            return

    log_name = os.path.join(env_ky.storage_path, '%s.log' % env_ky.genfile_name)
    log_msgs = search_match_files(env_ky.project_path)

    # 循环加密所有文件
    password = getpass.getpass()  # 获取输入密码
    for each_path in log_msgs.splitlines():
        encryption_file(r'%s' % each_path, password)

    # 写入到日志文件
    log_file = open(log_name, 'w')
    log_file.writelines(log_msgs)
    log_file.close()


if __name__ == '__main__':
    u"""测试.
    """

    config_update_datetime(__file__)
    env_ky.filter_type = ('.pyc',)
    env_ky.storage_path = os.path.join(env_ky.app_path, 'encrypts')

    encrypt_log_file()
