﻿#!usr/bin/env python
# coding: utf-8

u"""
    编译py文件到pyc文件的转换.

    usage:
        from .compile import compile_dir
"""

__author__ = 'kylinfish@126.com'
__date__ = '2013/8/15'

import os
import six
import compileall


def make_syspath():
    u"""构造默认下项目目录的路径.
    """

    # 放置在根目录下, 项目目录=父目录的父目录
    root_path = os.path.dirname(os.path.abspath(os.path.pardir))
    return root_path


def compile_dir(basedir=None, force=0, quiet=0):
    u"""编译项目文件目录下源码.
    
        :param basedir: 项目目录
        :param force:   if 1, force compilation, even if timestamps are up-to-date
        :param quiet:   if 1, be quiet during compilation
    """

    if basedir:
        compileall.compile_dir(basedir, force=force, quiet=quiet)
    else:
        basedir = make_syspath()
        compileall.compile_dir(basedir, force=force, quiet=quiet)


if __name__ == '__main__':
    u"""直接文件调用运行
    """

    six.print_(make_syspath())
    compile_dir()
