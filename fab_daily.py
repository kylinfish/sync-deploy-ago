#!usr/bin/env python
# coding: utf-8

u"""
    自动化日常任务.

    usage:
        fab --list -f fab_daily   -- 查看可用命令
        fab -R nginx nginx_restart  -- 启动 nginx
        fab -R mysql mysql_backup  -- 启动 mysql
"""

__author__ = 'kylinfish@126.com'
__date__ = '2013/8/15'

from fabric.api import *


@task
def nginx_restart():
    u"""nginx restart.
    """

    sudo('/etc/init.d/nginx restart')


@task
def mysql_backup():
    u"""mysql backup.
    """

    pass


if __name__ == '__main__':
    u"""测试
    """

    pass
