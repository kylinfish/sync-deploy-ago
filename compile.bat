﻿::	author: kylinfish@126.com
::	datetime: 2013.8.15
::  __doc__: code compiler

@echo off

echo --------------------------------------------------
echo --------------------------------------------------

echo 当前盘符：%~d0 
echo 当前盘符和路径：%~dp0 
echo 当前批处理全路径：%~f0 
echo 当前盘符和路径的短文件名格式：%~sdp0 
echo 当前CMD默认目录：%cd% 

echo --------------------------------------------------
echo --------------------------------------------------

::获取当前路径
set curpath=%~dp0

::测试路径获取
echo %curpath%
echo %curpath:~0,-1%

echo --------------------------------------------------
echo --------------------------------------------------

::echo 按任意键继续下步操作
pause

python -c "import compileall;compileall.compile_dir('%curpath:~0,-1%')"