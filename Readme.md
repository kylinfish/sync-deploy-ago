####简介:
        在12年的时候, 在内地二级城市, 参与了一个项目的开发过程, 在当时的环境, 发布流程完全为人工, 程序人
    员提交个人的改动, 在发布的当天, 每人将提交个文件给某成员, 该成员再通过sftp目录树的形式拖放上传, ssh
    的形式登陆重启web服务器, 紧接的是测试, 到生产环境.
    
        以上手工过程产生的问题有:
            1, 过程繁琐, 部署人员深陷文件比对, 文件逐数拖放的不厌其烦中.
            2, 程序人员, 每次额外地需要提交文件列表.
            3, 任何个的测试错误, 都有可能是程序人员, 部署人员缺失或遗漏某个文件所致.
            
        在部署自动化方面, 有太多的形式, 也有更好的选择, 该APP的初衷是用程序自动化的形式来模拟实现以上人工
    的过程, 仅此而已.
    
####注意:
    1, fabric 走 ssh 通道, 服务器要装 openssh-server.
    2, config.py 中的账户要有相应的权限.

####更新日志:
    2014.10.23
        修正创建目录可能的问题, 将单层 mkdir 改为 多层 makedirs.
        增添了发布前的备份, 以及发布失败后, 从备份回滚. 

####依赖包：
    fabric
    pycrypto

####安装包：
    pip install fabric
    pip install pycrypto

###功能模块文件集：

####打包：
    service/conf.py     打包环境配置项
    service/utils.py    打包代码工具箱
	service/collector.py    打包功能模块

####编译：
	service/compile.py  编译代码为字节码文件
	
######sh: 类unix执行 bat: windows执行	
	compile.bat  

	step-01-force-compile.sh
	step-01-non-force-compile.sh

	step-01-force-compile.bat
	step-01-non-force-compile.bat

####加密：(这块目前没搞明白, 待探索中...)
	encrypted.py    字节码文件加密
	fab_safe.py     遍历目录, 加密, 写入日志
	fab_safe.ini    指定时间点的文件
	
	step-02-encrypt.sh
	step-02-encrypt.bat

####上传：
    fab_upload_before.py    迭代更新前操作
    fab_upload_after.py     迭代更新后操作

	fab_upload.py   迭代更新
	fab_upload.ini  迭代更新时间点

	step-03-upload.sh
	step-03-upload.bat
	
####遗留: 
    迭代发布失败的回滚操作
    更新文件的打包是以zip的格式, 因为最早是在windows平台实现, 所以没改一直延续到现在
