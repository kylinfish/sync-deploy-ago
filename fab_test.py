#!usr/bin/env python
# coding: utf-8

u"""
    自动化部署, 本地到远程任务, 主要用于对 fabric 的探索.

    usage:fab function_name -f fab_test.py

    local('pwd')                     -- 执行本地命令
    lcd('/tmp')                      -- 切换本地目录
    cd('/tmp')                       -- 切换远程目录
    run('ls -a')                     -- 执行远程命令
    sudo('/etc/init.d/nginx start')  -- 执行远程 sudo, 注意pty选项
"""

__author__ = 'kylinfish@126.com'
__date__ = '2013/8/15'

import os
import six
import opcode
from fabric.api import *
from service.conf import env_ky
from config import remote_folder

if six.PY3:
    range_ = range
else:
    range_ = xrange


def test_print_user():
    u""" 打印出用户.
    """

    with hide('running'):
        run('echo "%(user)s"' % env.user)


def test_put_file():
    u""" 上传文件到远程, 并远程解压它.
    """

    if not os.path.exists(env_ky.storage_path):
        os.makedirs(env_ky.storage_path)

    # 切换到更新包存档目录
    with lcd(env_ky.storage_path):
        # 创建生成临时文件
        for i in range_(10):
            local('echo %s >> %s.txt' % ("hello world " * i, i))

        local('tar -czf test_put_file.tar.gz *.txt')
        local('rm *.txt')

        # 上传文件
        put('test_put_file.tar.gz', remote_folder)

    # 切换到远程, 解压
    with cd(remote_folder):
        run('tar zxf test_put_file.tar.gz')


def test_op_code():
    u""" opcode module.
    """

    six.print_(opcode.__file__)
    for op in range_(len(opcode.opname)):
        print('ox%.2X(%.3d): %s' % (op, op, opcode.opname[op]))


def test_local_run(kind=None):
    u"""读取内容.
    """

    if kind:
        log_one = local('tail backup_remote.log -n 1')
        six.print_(log_one)
        return log_one

    # 上次发布前的备份文件
    bck_log = None
    try:
        bck_log = open('%s/backup_remote.log' % env_ky.app_path, 'rb')
        backup_project_file_name = str(bck_log.readlines()[-1].replace("\n", ""))  # 可能的内容为空, 索引不存在异常.
    except:
        raise
    finally:
        if bck_log:
            bck_log.close()

    return backup_project_file_name


if __name__ == '__main__':
    u"""测试.
    """

    # test_put_file()
    cc = test_local_run()
    six.print_(cc)
    # pass
