#!usr/bin/env python
# coding: utf-8

u"""
    自动化部署, 本地到远程任务.

    usage: fab upload_before -f fab_upload_before.py
"""

__author__ = 'kylinfish@126.com'
__date__ = '2013/8/15'

import os
# import platform

from fabric.api import *

from service import env_ky
from service.utils import MyUtilsOS
from config import svn_url, remote_folder


def update_svn_windows():
    u"""如果项目使用windows平台下SVN, 发布前获取下更新.
    """

    # 注意: win下将svn增加到环境path中
    # 比如: D:\Program Files\TortoiseSVN\bin
    # TortoiseSVN 命令参考其帮助文档

    # 切换到项目源码目录
    with lcd(env_ky.project_path):
        # 从SVN地址更新项目源码
        local('TortoiseProc.exe /command:update /url:%s /path:%s /closeonend:1' % (svn_url, env_ky.project_path))


def backup_for_rollback():
    u"""远程文件打包备份, 用户迭代失败的回滚.

        以项目的父级目录备份操作.
    """

    backup_project_file_name = "backup_%s.tar.gz" % env_ky.gen_pkg_name
    remote_project_parent_dir = os.path.dirname(remote_folder)

    # 远程文件打包备份
    with cd(remote_project_parent_dir):  # 切换到远程目录
        if MyUtilsOS.is_file_in_folder(remote_project_parent_dir):
            run('tar -czf %s %s' % (backup_project_file_name, remote_folder))  # 远程项目打包备份

    # 远程备份记录
    # 这次发布前的备份文件
    with lcd(env_ky.app_path):
        local('echo %s >> backup_remote.log' % backup_project_file_name)


def upload_before():
    u"""迭代文件上传之后执行操作集合.
    """

    # update_svn_windows()
    backup_for_rollback()
    # pass


if __name__ == '__main__':
    u"""测试.
    """

    pass
